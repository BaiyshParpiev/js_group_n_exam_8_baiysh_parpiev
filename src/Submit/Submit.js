import React, {useState} from 'react';
import axiosApi from "../axiosApi";
import Input from "../Components/Input/Input";
import Spinner from "../Components/Spinner/Spinner";

const Submit = ({history}) => {
    const [input, setInput] = useState({
        quote: '',
        author: '',
    });
    const [loading, setLoading] = useState(false)

    const change = e => {
        const {name, value} = e.target;

        setInput(prev => ({
            ...prev,
            [name]: value
        }));
    }

    const handleSelect=(e)=>{
        setInput(prev => ({
            ...prev,
            category: e
        }));
    }

    const onSubmit = async e => {
        e.preventDefault();
        setLoading(true)
        try {
            await axiosApi.post('/quotes.json', {
                ...input,
            });
        } finally {
            setLoading(false)
            history.replace('/all');
        }
    }

    return (loading ? <Spinner/> : <Input change={change} onSubmit={onSubmit} quote={input.quote} author={input.author} category={input.category} handleSelect={handleSelect}/>);
}

export default Submit;


