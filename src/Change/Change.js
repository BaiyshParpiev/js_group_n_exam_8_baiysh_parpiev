import React, {useEffect, useState} from 'react';
import axiosApi from "../axiosApi";
import Input from "../Components/Input/Input";
import Spinner from "../Components/Spinner/Spinner";

const Change = ({match, history}) => {
    const [loading, setLoading] = useState(false)
    const [blog, setBlog] = useState({});

    const change = e => {
        const {name, value} = e.target;
        setBlog(prev => (
            {
                ...prev,
                [name]: value,
            }));
    }

    useEffect(() => {
        const fetchData = async () => {
            const response = await axiosApi.get('/quotes/' + match.params.quoteId + '.json');
            setBlog(response.data);
        }

        fetchData().catch(e => console.log(e));
    }, [match.params.quoteId]);

    const handleSelect=(e)=>{
        setBlog(prev => ({
            ...prev,
            category: e
        }));
    }

    const onSubmit = async e => {
        e.preventDefault();
        setLoading(true)
        try {
            await axiosApi.put('/quotes/' + match.params.quoteId + '.json', {
                ...blog,
            });

        } finally {
            setLoading(false)
            history.replace('/');
        }
    }
    return (loading ? <Spinner/> : <Input change={e => change(e)} onSubmit={onSubmit} quote={blog.quote} author={blog.author} category={blog.category} handleSelect={handleSelect}/>);

};

export default Change;