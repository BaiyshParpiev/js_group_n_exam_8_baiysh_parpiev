import React from 'react';
import {BrowserRouter, NavLink, Route, Switch} from 'react-router-dom';
import logo from './logo.svg';
import './App.css';
import Submit from "./Submit/Submit";
import Quotes from "./Quotes/Quotes";
import Change from "./Change/Change";


const App = () => {

    return (
        <BrowserRouter>
            <div className="App">
                <header className="App-header">
                    <div className="container header">
                        <a href="https://shop.mango.com/" className="header__logo">
                            <img src={logo} className="App-logo" alt="logo"/>
                        </a>
                        <div className="header__links">
                            <ul>
                                <NavLink to="/">Quotes</NavLink>
                                <NavLink to="/submit">Submit new quotes</NavLink>
                            </ul>
                        </div>
                    </div>
                </header>
                <div>
                    <Switch>
                        <Route path="/" exact component={Quotes}/>
                        <Route path="/all"  component={Quotes}/>
                        <Route path="/submit" component={Submit}/>
                        <Route path="/quotes/:quoteId/edit" component={Change}/>
                        <Route path='/quotes/:handle' component={Quotes}/>
                        <Route path='/quotes/:handle' component={Quotes}/>
                        <Route path='/quotes/:handle' component={Quotes}/>
                        <Route path='/quotes/:handle' component={Quotes}/>
                        <Route path='/quotes/:handle' component={Quotes}/>
                        <Route render={() => <h1>Not found</h1>}/>
                    </Switch>
                </div>
                <footer className="footer">
                    <div className="container">
                        <div className="footer__top row">
                            <div className="footer__mail ">
                                <a href="https//:bayish.parpiev@gmail.com" className="info">baiysh.parpiev@gmail.com</a>
                            </div>
                            <div className="footer__phone">
                                <a href="https//:instagram.com" className="info">(+49) 1722962698</a>
                                <span className="footer__border"/>
                            </div>
                        </div>
                        <div className="footer__bottom">
                            <p className="web-info">Copyright(c) website name. <span className="web-info__text">Designed by: www.alltemplateneeds.com</span> /
                                Images from: www.wallpaperswide.com, www.photorack.net</p>
                        </div>
                    </div>
                </footer>
            </div>

        </BrowserRouter>
    )
}


export default App;
