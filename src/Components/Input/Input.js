import React from 'react';
import Button from "../Button/Button";
import DropdownButton from 'react-bootstrap/DropdownButton';
import Dropdown from 'react-bootstrap/Dropdown'

const Input = (props) => {
    return (
        <form onSubmit={props.onSubmit}>
            <DropdownButton
                alignRight
                title="Category"
                id="dropdown-menu-align-right"
                onSelect={props.handleSelect}
            >
                <Dropdown.Item eventKey="Star-Wars">Star Wars</Dropdown.Item>
                <Dropdown.Item eventKey="Famous-people">Famous people</Dropdown.Item>
                <Dropdown.Item eventKey="Saying">Saying</Dropdown.Item>
                <Dropdown.Item eventKey="Humour">Humour</Dropdown.Item>
                <Dropdown.Item eventKey="Motivational">Motivational</Dropdown.Item>
                <Dropdown.Divider />
            </DropdownButton>
            <span className="input-group-text" id="inputGroup-sizing-default">{props.category}</span>
            <div className="input-group mb-3 mt-4 pt-4">
                <div className="input-group-prepend">
                    <span className="input-group-text" id="inputGroup-sizing-default">Author</span>
                </div>
                <input type="text" name='author' className="form-control" onChange={props.change}
                       aria-label="Sizing example input"
                       aria-describedby="inputGroup-sizing-default" value={props.author}/>
            </div>
            <div className="input-group">
                <div className="input-group-prepend">
                    <span className="input-group-text">Description</span>
                </div>
                <textarea className="form-control" name='quote' onChange={props.change} aria-label="With textarea" value={props.quote}/>
            </div>
            <Button name='Submit'/>
        </form>
    );
};

export default Input;