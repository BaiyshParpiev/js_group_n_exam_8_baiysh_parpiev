import React, {useEffect, useState} from 'react';
import axiosApi from "../axiosApi";
import Button from "../Components/Button/Button";
import {NavLink} from "react-router-dom";
import Spinner from "../Components/Spinner/Spinner";

const Quotes = ({match, history}) => {
    const [quote, setQuote] = useState(null);
    const [loading, setLoading] = useState(false)


    useEffect(() => {
        const fetchData = async() => {
            if(match.path === '/' || match.path === '/all'){
                const response = await axiosApi.get( '/quotes/.json');
                setQuote(response.data)
            }else{
                const response = await axiosApi.get( `/quotes.json?orderBy="category"&equalTo="${match.params.handle}"`);
                setQuote(response.data)
            }
        }
        fetchData().catch(e => console.log(e));
    },[match.path, match.params.handle]);

    const quotes = () => {
        const arr = [];
        for(const key in quote){
            arr.push(quote[key])
        }
        return arr;
    }

    const quoteArr = quotes();
    const changeSide = index => {
        const key = Object.keys(quote)
        history.replace('/quotes/' + key[index] + '/edit')
    }
    const onDelete = async (index)  => {
        setLoading(true)
        const key = Object.keys(quote)
        try {
            await axiosApi.delete( '/quotes/' + key[index] + '.json');
        } finally {
            setLoading(false)
           if(match.path === '/all'){
               history.replace('/');
           } history.replace('./all')
        }
    }

    return (
       loading ? <Spinner/> :  <div className='d-flex flex-row block container'>
           <div>
               <ul className="d-flex flex-column">
                   <NavLink to='/all'>All</NavLink>
                   <NavLink to='/quotes/Star-Wars'>Star Wars</NavLink>
                   <NavLink to='/quotes/Humour'>Humour</NavLink>
                   <NavLink to='/quotes/Saying'>Saying</NavLink>
                   <NavLink to='/quotes/Famous-people'>Famous People</NavLink>
                   <NavLink to='/quotes/Motivational'>Motivational</NavLink>
               </ul>
           </div>
           <div className="home">
               {quoteArr.map(b => (<div className="m-4 p-4 border border-success" key={quoteArr.indexOf(b)}><p className='border border-primary rounded p-4'>{b.quote}</p><Button name='Delete' click={() => onDelete(quoteArr.indexOf(b))}/><Button name='Edit' click={() => changeSide(quoteArr.indexOf(b))}/></div>))}
           </div>
       </div>
    );
};

export default Quotes;